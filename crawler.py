import sqlite3
import requests
from bs4 import BeautifulSoup
import database as db
import networkx as nx


class Crawler:

    def parsed_html(self, url):
        code = requests.get(url)
        status = (code.status_code)
        plain = code.text
        s = BeautifulSoup(plain, "html.parser")

        return status, plain, s

    def get_urls(self, s, Web_Urls):
        home_url = 'https://unsplash.com'
        for link in s.findAll('a'):
            if link.get('href') not in Web_Urls:
                l = link.get('href')
                if l is not None and l[0] == '/' and l != '/':
                    Web_Urls.append(home_url+l)
        return Web_Urls

    def web(self, Web_Urls, mydb, table_name):
        i = 0
        j = 0
        while i < 100:
            url = Web_Urls[j]
            #print(i, "  ", j, "   ", url)
            status, plain, s = self.parsed_html(url)
            if len(Web_Urls) <= 100:
                WebUrls = self.get_urls(s, Web_Urls)
            if db.check_url_in_db(mydb, table_name, Web_Urls[j]):
                db.insert_webpage_in_db(
                    mydb, table_name, Web_Urls[j], plain, status)
                i += 1
                j += 1
            else:
                j += 1
                continue

    def print_data(self, mydb, table_name):
        results = db.fetch_all_rows_in_table(mydb, table_name)
        for result in results:
            print(result)

    def link_map(self, Web_Urls, mydb, table_name):
        i = 0
        j = 0
        while i < 5000:
            urls = set(Web_Urls)
            url = list(urls)[j]
            code = requests.get(url)
            s = BeautifulSoup(code.text, "html.parser")
            links = self.get_urls(s, [url])
            for link in links:
                db.insert_in_url_map(mydb, table_name, url, link)
                i += 1
                print(i, "  row inserted source = ",
                      url, "   destination  = ", link, "no of link ", len(links))
            Web_Urls = Web_Urls + links
            j += 1

    def add_nodes(self, mydb, table_name):
        l = []
        results = db.get_nodes(mydb, table_name)
        for i in range(len(results)):
            l.append(results[i][0])
        return l

    def add_edges(self, mydb, table_name):
        l = []
        results = db.get_edges(mydb, table_name)
        for i in range(len(results)):
            l.append((results[i][0], results[i][1]))
        return l

    def create_network(self, mydb, table_name, source, destination):
        G = nx.Graph()
        G.add_nodes_from(self.add_nodes(mydb, table_name))
        G.add_edges_from(self.add_edges(mydb, table_name))
        path = nx.shortest_path(G)
        distance = dict(nx.shortest_path_length(G))
        return path, distance
        # print(path[source][destination])
        # print(distance[source][destination])
        '''all_shortest_path = dict(nx.all_pairs_shortest_path(G))
        print(all_shortest_path)'''
