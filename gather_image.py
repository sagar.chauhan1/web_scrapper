import requests
from bs4 import BeautifulSoup
import database as db
import os
import urllib.request
from PIL import Image
import numpy as np


def get_soup_obj(url):
    code = requests.get(url)
    plain = code.text
    s = BeautifulSoup(plain, "html.parser")
    return s


def get_links(WebUrl, paths, tag, tag1, tag_val, ext_tag):
    links = []
    for path in paths:
        url = WebUrl + path
        s = get_soup_obj(url)
        for link in s.findAll(tag, {tag1: tag_val}):
            if link.get(ext_tag) is not None:
                links.append(link.get(ext_tag))
    return links


def get_image(mydb, table_name, WebUrl, paths, tag, tag1, tag_val, ext_tag):
    links = []
    counter = 0
    for path in paths:
        print(path)
        url = WebUrl + path[0]
        s = get_soup_obj(url)
        for link in s.findAll(tag, {tag1: tag_val}):
            if link.get(ext_tag) is not None:
                counter += 1
                store_image(mydb, table_name, link.get(
                    ext_tag), path[0], counter)
                links.append(link.get(ext_tag))
    return links


def store_image(mydb, table_name, srcs, tags, i):
    # for i in range(len(srcs)):
    img_path = "images/"+str(i)+".jpg"
    # print(srcs[i])
    urllib.request.urlretrieve(srcs, img_path)
    db.insert_image_tag(mydb, table_name, img_path, tags)
    #print(i+1, "  row insterd")


def get_tag(WebUrl, paths, tag, tag1, tag_val):
    tags = []
    for path in paths:
        url = WebUrl + path
        s = get_soup_obj(url)
        for link in s.findAll(tag, {tag1: tag_val}):
            if link.contents is not None:
                tags.append(link.contents)
    return tags


def gather_images_tags(mydb, table_name, WebUrl):
    path = ['/explore']
    db.create_img_table(mydb, table_name)
    paths = get_links(WebUrl, path, 'a', 'class',
                      '_2634o _1CBrG _1ByhS _3tN3z', 'href')
    tags = get_tag(WebUrl, paths, 'a', 'class', '_6PxCM')
    WebUrl = 'https://unsplash.com/search/photos/'
    srcs = get_image(mydb, table_name, WebUrl, tags,
                     'img', 'class', '_2zEKz', 'src')
    print(len(tags))
    s = set(tags)


def process_data(mydb, table_name):
    results = db.get_image_tag(mydb, table_name)
    tag = []
    ext = '.jpg'
    file = []
    ablob = []
    img_array = []
    for i in range(len(results)):
        ablob.append(results[i][1])
        tag.append(results[i][4])
        file.append(results[i][3])
        filename = results[i][3] + ext
        with open(filename, 'wb') as output_file:
            output_file.write(results[i][1])
        img = Image.open(filename)
        # img.show()
        img.load()
        data = np.asarray(img, dtype="int32")
        img_array.append(data)
    # print(len(img_array))
    return tag, img_array
