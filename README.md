** Web Crawler**
```
1: It crawl HTML comtent from site and insert to tha database
2: find Shortest path between links
3: download all the images and store in the databse
```
## Creating Virtual evn
```
virtualenv env-name
source env-name/bin/activate
```
## installing required modules
```
pip3 install -r requirements.txt
```
## for Crawling the HTML Content
```
python3 main.py -add_data True      or
python3 main.py

```

## forretrieving html content from Database
```
python3 main.py -get_data True

```

## for creating url graph
```
python3 main.py -create_url_map True 

```

## for finding shortest path
```
python3 main.py -shortest_path True 

```

## for Creating Image Database
```
python3 main.py -create_image_database True  

```

## for Trainig Image Classifier
```
python3 main.py -train_nn_model True  

```
