import crawler as cr
import database as db
import argparse
import gather_image as imgcls
import classification as clf


def get_data(obj, mydb, table_name):
    obj.print_data(mydb, table_name)


def add_data(obj, mydb, table_name,url):
    db.create_table(mydb, table_name)
    urls = [url]
    obj.web(urls, mydb, table_name)


def main(args):
    obj = cr.Crawler()
    mydb = db.connect_db()
    table_name = "html_content"
    url = 'https://unsplash.com'
    url_map_table = 'url_map1'
    image_table_name = "PICTURES1"
    if eval(args.add_data):
        add_data(obj, mydb, table_name, url)
    if eval(args.get_data):
        get_data(obj, mydb, table_name)
    if eval(args.create_url_map):
        db.create_url_map_table(mydb, url_map_table)
        obj.link_map(['https://unsplash.com'], mydb, url_map_table)
    if eval(args.shortest_path):
        source = raw_input("Enter The Source Url : =>")
        destination = raw_input("Enter the destination Url =>")
        path, distance = obj.create_network(
            mydb, url_map_table, source, destination)
        print(path, "   ", distance)
    if eval(args.create_image_database):
        imgcls.gather_images_tags(mydb, image_table_name,url)
    if eval(args.train_nn_model) is True:
        clf.nn(mydb, image_table_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-add_data', default='True', help="True Or False")
    parser.add_argument('-get_data', default='False', help="True or False")
    parser.add_argument('-create_url_map', default='False',
                        help="True or False")
    parser.add_argument('-shortest_path', default='False',
                        help="True or False")
    parser.add_argument('-create_image_database',
                        default='False', help="True or False")
    parser.add_argument('-train_nn_model', default='False',
                        help="True or False")
    args = parser.parse_args()
    main(args)
