import sqlite3
import os


def fetch_all_rows_in_table(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute("SELECT * from "+table_name)
    rows = cursor.fetchall()
    keys = ('status', 'url', 'content')
    results = []
    for row in rows:
        results.append(dict((keys[i], row[i]) for i in range(3)))
    return results


def insert_webpage_in_db(mydb, table_name, link, content, status):
    cursor = mydb.cursor()
    cursor.execute('''INSERT INTO html_content (status, url, content) VALUES
         (?,?,?)''', (status, link, content))
    mydb.commit()


def connect_db():
    mydb = sqlite3.connect('webpage.db')
    return mydb


def create_table(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS html_content
     (status INTEGER, url VARCHAR PRIMARY KEY, content VARCHAR );''')


def check_url_in_db(mydb, table_name, url):
    cursor = mydb.cursor()
    cursor.execute(
        "SELECT count(url) from html_content WHERE url = '"+url+"';")
    x = cursor.fetchall()[0][0]
    if x == 0:
        return True
    else:
        return False


def create_url_map_table(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS url_map1
     ( source VARCHAR, destination VARCHAR);''')


def insert_in_url_map(mydb, table_name, source, destination):
    cursor = mydb.cursor()
    cursor.execute('''INSERT INTO url_map1 (source, destination) VALUES
         (?,?)''', (source, destination))
    mydb.commit()


def get_nodes(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute("SELECT distinct destination from "+table_name)
    results = cursor.fetchall()
    return results


def get_edges(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute("SELECT * from "+table_name)
    results = cursor.fetchall()
    return results


def create_img_table(mydb, table_name):
    cursor = mydb.cursor()
    sql = '''create table if not exists PICTURES1(
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        PICTURE BLOB,TYPE TEXT,
        FILE_NAME TEXT,TAG VARCHAR);'''
    cursor.execute(sql)


def insert_image_tag(mydb, table_name, img_path, tag):
    with open(img_path, 'rb') as input_file:
        cursor = mydb.cursor()
        ablob = input_file.read()
        base = os.path.basename(img_path)
        afile, ext = os.path.splitext(base)
        cursor.execute('''INSERT INTO PICTURES1 (PICTURE, TYPE, FILE_NAME, TAG) VALUES(?, ?, ?,?);''', [
                       sqlite3.Binary(ablob), ext, afile, tag])
        mydb.commit()


def get_image_tag(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute("SELECT * from "+table_name)
    results = cursor.fetchall()
    return results
